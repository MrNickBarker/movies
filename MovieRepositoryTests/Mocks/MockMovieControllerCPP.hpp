#ifndef MockMovieControllerCPP_h
#define MockMovieControllerCPP_h

#include "MovieControllerCPP.hpp"

namespace movies {
    class MockMovieController: public MovieController {
    public:
        std::vector<Movie*> mockMovies;
        MovieDetail *mockDetail;

        std::vector<Movie*> getMovies() {
            return mockMovies;
        }

        MovieDetail* getMovieDetail(std::string name) {
            return mockDetail;
        }
    };
}

#endif /* MockMovieControllerCPP_h */
