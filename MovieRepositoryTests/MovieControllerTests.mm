#import <XCTest/XCTest.h>
#import "MockMovieControllerCPP.hpp"
#import "MovieController.h"
#import "MovieController_Internal.h"
#import <MovieRepository/MovieRepository-Swift.h>

@interface MovieControllerTests : XCTestCase
@property (nonatomic, strong) MovieController *wrapper;
@property (nonatomic) movies::MockMovieController *mockMovieController;
@end

@implementation MovieControllerTests

- (void)setUp {
    [super setUp];
    self.wrapper = [MovieController new];
    self.mockMovieController = new movies::MockMovieController();
    [self.wrapper setMovieController:self.mockMovieController];
}

- (void)tearDown {
    delete self.mockMovieController;
    [super tearDown];
}

- (void)test_convertingMovies {
    auto m1 = new movies::Movie();
    m1->name = "Test movie";
    m1->lastUpdated = 123;
    self.mockMovieController->mockMovies.push_back(m1);

    auto m2 = new movies::Movie();
    m2->name = "Other movie";
    self.mockMovieController->mockMovies.push_back(m2);

    XCTestExpectation *exp = [self expectationWithDescription:@"Wait for the background thread"];
    __block NSArray<Movie*> *movies;

    [self.wrapper getMovies:^(NSArray<Movie *> * _Nullable results) {
        movies = results;
        [exp fulfill];
    }];

    [self waitForExpectationsWithTimeout:0.1 handler:nil];
    XCTAssertEqual(movies.count, 2);
    XCTAssert([movies[0].name isEqual:@"Test movie"]);
    XCTAssert(movies[0].lastUpdated == 123);
    XCTAssert([movies[1].name isEqual:@"Other movie"]);
    XCTAssert(movies[1].lastUpdated == 0);
}

- (void)test_convertingDetail_withoutActors {
    auto movieDetail = new movies::MovieDetail();
    movieDetail->name = "Movie name";
    movieDetail->score = 2.5;
    movieDetail->description = "Movie description";

    self.mockMovieController->mockDetail = movieDetail;

    XCTestExpectation *exp = [self expectationWithDescription:@"Wait for the background thread"];
    __block MovieDetail *detail;

    [self.wrapper getDetailsForMovie:@"Movie name" callback:^(MovieDetail * _Nullable result) {
        detail = result;
        [exp fulfill];
    }];

    [self waitForExpectationsWithTimeout:0.1 handler:nil];
    XCTAssert([detail.name isEqual:@"Movie name"]);
    XCTAssert([detail.summary isEqual:@"Movie description"]);
    XCTAssert(detail.score == 2.5);
    XCTAssert(detail.actors.count == 0);
}

- (void)test_convertingDetail_withActors {
    auto movieDetail = new movies::MovieDetail();
    movieDetail->name = "Movie with actors";

    auto a1 = movies::Actor();
    a1.name = "Actor 1";
    a1.imageUrl = "image-url";
    a1.age = 50;
    movieDetail->actors.push_back(a1);

    auto a2 = movies::Actor();
    a2.name = "Actor 2";
    movieDetail->actors.push_back(a2);

    self.mockMovieController->mockDetail = movieDetail;

    XCTestExpectation *exp = [self expectationWithDescription:@"Wait for the background thread"];
    __block MovieDetail *detail;

    [self.wrapper getDetailsForMovie:@"" callback:^(MovieDetail * _Nullable result) {
        detail = result;
        [exp fulfill];
    }];

    [self waitForExpectationsWithTimeout:0.1 handler:nil];
    XCTAssert([detail.name isEqual:@"Movie with actors"]);
    XCTAssert([detail.summary isEqual:@""]);
    XCTAssert(detail.score == 0);
    XCTAssert(detail.actors.count == 2);
    XCTAssert([detail.actors[0].name isEqual:@"Actor 1"]);
    XCTAssert([detail.actors[0].imageUrl.absoluteString isEqual:@"image-url"]);
    XCTAssert(detail.actors[0].age == 50);
    XCTAssert([detail.actors[1].name isEqual:@"Actor 2"]);
    XCTAssert(detail.actors[1].age == 0);
    XCTAssertNil(detail.actors[1].imageUrl);
}

- (void)test_gettingDetail_forNonExistentMovie {
    XCTestExpectation *exp = [self expectationWithDescription:@"Wait for the background thread"];
    __block MovieDetail *detail = [[MovieDetail alloc] initWithName:@"" summary:@"" score:0 actors:nil];

    [self.wrapper getDetailsForMovie:@"it's not there" callback:^(MovieDetail * _Nullable result) {
        detail = result;
        [exp fulfill];
    }];

    [self waitForExpectationsWithTimeout:0.1 handler:nil];
    XCTAssertNil(detail);
}

@end
