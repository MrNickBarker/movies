import XCTest
@testable import Services

class ImageCacheTests: XCTestCase {
    var network: MockNetworkClient!
    var imageCache: ImageCache!

    override func setUp() {
        network = MockNetworkClient()
        imageCache = ImageCache(network: network)
    }

    func test_loadingImage_withError_returnsNoImage() {
        network.error = CocoaError.error(.userCancelled)
        var returnedImage: UIImage? = UIImage()

        imageCache.imageFor(url: URL(string: "url")!) { image in
            returnedImage = image
        }

        XCTAssertNil(returnedImage)
    }

    func test_loadingImage_wrongDataFormat_returnsNoImage() {
        network.data = "should be an image not a string".data(using: .utf8)
        var returnedImage: UIImage? = UIImage()

        imageCache.imageFor(url: URL(string: "url")!) { image in
            returnedImage = image
        }

        XCTAssertNil(returnedImage)
    }

    func test_loadingImage_withImageData_returnsImage() {
        let url = URL(string: "url")!
        network.data = UIImage(systemName: "star")?.pngData()
        var returnedImage: UIImage? = nil

        imageCache.imageFor(url: url) { image in
            returnedImage = image
        }

        XCTAssertNotNil(returnedImage)
        XCTAssertEqual(network.urls, [url])
    }

    func test_loadingSameImage_usesLocalCacheNotNework() {
        let url = URL(string: "url")!
        network.data = UIImage(systemName: "star")?.pngData()
        var returnedImage: UIImage? = nil

        imageCache.imageFor(url: url) { _ in }
        imageCache.imageFor(url: url) { image in
            returnedImage = image
        }

        XCTAssertNotNil(returnedImage)
        XCTAssertEqual(network.urls, [url])
    }
}
