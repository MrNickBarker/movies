import Foundation
@testable import Services

class MockNetworkClient: Networking {
    var data: Data?
    var response: URLResponse?
    var error: Error?
    var urls: [URL] = []

    func getData(from url: URL, callback: @escaping (Data?, URLResponse?, Error?) -> Void) {
        urls.append(url)
        callback(data, response, error)
    }
}
