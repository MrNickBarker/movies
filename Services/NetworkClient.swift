import Foundation

protocol Networking {
    func getData(from url: URL, callback: @escaping (Data?, URLResponse?, Error?) -> Void)
}

class NetworkClient: Networking {
    private let urlSession: URLSession
    private let callbackQueue: DispatchQueue

    init(urlSession: URLSession = .shared, callbackQueue: DispatchQueue = .main) {
        self.urlSession = urlSession
        self.callbackQueue = callbackQueue
    }

    func getData(from url: URL, callback: @escaping (Data?, URLResponse?, Error?) -> Void) {
        urlSession.dataTask(with: url) { [weak self] data, response, error in
            self?.callbackQueue.async {
                callback(data, response, error)
            }
        }.resume()
    }
}
