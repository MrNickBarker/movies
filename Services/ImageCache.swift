import UIKit

public protocol ImageCaching {
    func imageFor(url: URL, callback: @escaping (UIImage?) -> Void)
}

public class ImageCache: ImageCaching {
    private let network: Networking
    private var cache: [URL: UIImage] = [:]

    init(network: Networking = NetworkClient()) {
        self.network = network
    }

    public func imageFor(url: URL, callback: @escaping (UIImage?) -> Void) {
        if let image = cache[url] {
            callback(image)
            return
        }
        network.getData(from: url) { data, _, error in
            guard error == nil else {
                callback(nil)
                return
            }
            if let data = data, let image = UIImage(data: data) {
                self.cache[url] = image
                callback(image)
            }
            else {
                callback(nil)
            }
        }
    }
}
