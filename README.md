# Movies
A simple iOS app showcasing data communication with C++.

- Simple UI, dynamic type, dark mode, all orientations and tablet.
- Unit tests for critical components.
- Modularised architecture.

## Modules
- Movies: Main app.
- MovieRepository: C++ wrapper.
- Services: Services such as image caching and networking.