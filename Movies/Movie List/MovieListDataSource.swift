import Foundation
import Services

class MovieListDataSource {

    struct Notification {
        static let stateUpdated = NSNotification.Name("MLDS:updatedState")
    }

    enum State {
        case uninitialized, loading, loaded(movies: [Movie])
    }

    private(set) var state: State = .uninitialized {
        didSet {
            NotificationCenter.default.post(name: Notification.stateUpdated, object: self)
        }
    }
    private let movieController: MovieControlling

    init(movieController: MovieControlling = MovieController()) {
        self.movieController = movieController
    }
}

extension MovieListDataSource {

    func refresh() {
        if case .loading = state { return }

        state = .loading
        movieController.getMovies { [weak self] movies in
            self?.state = .loaded(movies: movies ?? [])
        }
    }

    func detailDataSource(for movie: Movie, imageCache: ImageCaching = ServiceRegistry.shared.imageCache) -> MovieDetailDataSource {
        return MovieDetailDataSource(movieName: movie.name, movieController: movieController, imageCache: imageCache)
    }
}
