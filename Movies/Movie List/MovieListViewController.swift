import UIKit

class MovieListViewController: UIViewController {
    private struct Constants {
        static let cellId = "cell"
    }

    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var loadingIndicator: UIActivityIndicatorView!

    private var movies: [Movie] = []
    private let dataSource = MovieListDataSource()
    private let timeFormatter = RelativeTimeFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(onDataSourceStateUpdated), name: MovieListDataSource.Notification.stateUpdated, object: nil)
        dataSource.refresh()
    }

    @objc func onDataSourceStateUpdated() {
        switch dataSource.state {
        case .uninitialized, .loading:
            movies = []
            tableView.isHidden = true
            loadingIndicator.startAnimating()
        case .loaded(let loadedMovies):
            movies = loadedMovies
            tableView.isHidden = false
            loadingIndicator.stopAnimating()
        }
        tableView.reloadData()
    }
}

extension MovieListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellId) ?? UITableViewCell(style: .subtitle, reuseIdentifier: Constants.cellId)
        let movie = movies[indexPath.row]
        cell.textLabel?.text = movie.name
        cell.detailTextLabel?.text = "Updated: \(timeFormatter.relativeTimeFor(timeInterval: movie.lastUpdated))"
        cell.detailTextLabel?.textColor = .gray
        return cell
    }
}

extension MovieListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let movieDetailVC = MovieDetailViewController.fromStoryboard()
        movieDetailVC.dataSource = dataSource.detailDataSource(for: movies[indexPath.row])
        navigationController?.pushViewController(movieDetailVC, animated: true)
    }
}
