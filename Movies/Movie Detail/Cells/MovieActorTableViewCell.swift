import UIKit

class MovieActorTableViewCell: UITableViewCell, RemoteImageDisplaying {
    var remoteImageTrackingId: String?

    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var headshotView: UIImageView!

    func setup(name: String, age: Int) {
        let text = NSMutableAttributedString(string: name, attributes: [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)])
        let age = NSAttributedString(string: " (\(age))", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.footnote)])
        text.append(age)
        nameLabel.attributedText = text
    }

    func loaded(image: UIImage?) {
        headshotView.image = image
    }
}
