import UIKit

class MovieTitleTableViewCell: UITableViewCell {
    var title: String? {
        set {
            titleLabel.text = newValue
        }
        get {
            return titleLabel.text
        }
    }
    @IBOutlet private var titleLabel: UILabel!
}
