import UIKit

class MovieTextTableViewCell: UITableViewCell {
    var contentText: String? {
        set {
            label.text = newValue
        }
        get {
            return label?.text
        }
    }
    @IBOutlet private var label: UILabel!
}
