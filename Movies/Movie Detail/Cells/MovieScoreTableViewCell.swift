import UIKit

class MovieScoreTableViewCell: UITableViewCell {
    @IBOutlet private var container: UIStackView!

    func setup(score: Float) {
        let subviews = container.subviews
        subviews.forEach { $0.removeFromSuperview() }

        var currentScore = score
        for _ in 0..<5 {
            if currentScore >= 1 {
                addStar(image: "star.fill")
            }
            else if currentScore >= 0.5 {
                addStar(image: "star.lefthalf.fill")
            }
            else  {
                addStar(image: "star")
            }
            currentScore -= 1
        }
    }

    private func addStar(image: String) {
        let imageView = UIImageView(image: UIImage(systemName: image))
        imageView.tintColor = .gray
        container.addArrangedSubview(imageView)
    }
}
