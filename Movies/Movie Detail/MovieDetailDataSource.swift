import UIKit
import Services

class MovieDetailDataSource {

    struct Notification {
           static let stateUpdated = NSNotification.Name("MDDS:updatedState")
   }

    enum Item: Equatable {
        case title(String)
        case score(Float)
        case text(String)
        case actor(Actor)
    }

    enum State {
        case loading([Item])
        case loaded([Item])
    }

    private let movieController: MovieControlling
    private let imageCache: ImageCaching
    private let movieName: String
    private(set) var state: State {
        didSet {
            NotificationCenter.default.post(name: Notification.stateUpdated, object: self)
        }
    }

    init(movieName: String, movieController: MovieControlling, imageCache: ImageCaching) {
        self.movieController = movieController
        self.imageCache = imageCache
        self.movieName = movieName
        self.state = .loading([
            .title(movieName),
            .score(0)
        ])
    }

    func loadDetails() {
        movieController.getDetailsForMovie(movieName) { [weak self] details in
            var items: [Item] = [
                .title(details?.name ?? self?.movieName ?? ""),
                .score(details?.score ?? 0),
                .text(details?.summary ?? "There is no summary for this movie")
            ]

            if details?.actors?.count ?? 0 > 0 {
                items.append(.text("Cast"))
            }

            details?.actors?.forEach {
                items.append(.actor($0))
            }

            self?.state = .loaded(items)
        }
    }

    func loadImageFor(url: URL?, in view: RemoteImageDisplaying) {
        view.loaded(image: nil)

        guard let url = url else { return }

        let trackingId = UUID().uuidString
        view.remoteImageTrackingId = trackingId

        imageCache.imageFor(url: url) { [weak view, trackingId] image in
            guard view?.remoteImageTrackingId == trackingId else { return }
            view?.loaded(image: image)
        }
    }
}
