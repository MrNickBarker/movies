import UIKit

class MovieDetailViewController: UIViewController, StoryboardInstantiantable {
    var dataSource: MovieDetailDataSource?
    @IBOutlet private var tableView: UITableView!
    private var items: [MovieDetailDataSource.Item] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(onDataSourceStateUpdated), name: MovieDetailDataSource.Notification.stateUpdated, object: nil)
        onDataSourceStateUpdated()
        dataSource?.loadDetails()
    }

    @objc func onDataSourceStateUpdated() {
        if let dataSource = dataSource {
            switch dataSource.state {
            case .loaded(let items), .loading(let items):
                self.items = items
            }
        }
        tableView.reloadData()
    }
}

extension MovieDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch items[indexPath.row] {
        case .title(let title):
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell") as! MovieTitleTableViewCell
            cell.title = title
            return cell
        case .score(let score):
            let cell = tableView.dequeueReusableCell(withIdentifier: "scoreCell") as! MovieScoreTableViewCell
            cell.setup(score: score)
            return cell
        case .text(let text):
            let cell = tableView.dequeueReusableCell(withIdentifier: "textCell") as! MovieTextTableViewCell
            cell.contentText = text
            return cell
        case .actor(let actor):
            let cell = tableView.dequeueReusableCell(withIdentifier: "actorCell") as! MovieActorTableViewCell
            cell.setup(name: actor.name, age: actor.age)
            dataSource?.loadImageFor(url: actor.imageUrl, in: cell)
            return cell
        }
    }
}
