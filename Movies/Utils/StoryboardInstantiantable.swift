import UIKit

protocol StoryboardInstantiantable {
    static var storyboard: String { get }
    static var storyboardIdentifier: String { get }

    static func fromStoryboard() -> Self
}

extension StoryboardInstantiantable {
    static var storyboard: String { "Main" }
    static var storyboardIdentifier: String { String(describing: self) }

    static func fromStoryboard() -> Self {
        return UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(identifier: storyboardIdentifier) as! Self
    }
}
