import Foundation

class RelativeTimeFormatter {
    private struct Time {
        static let hour: TimeInterval = 60*60
        static let day: TimeInterval = 60*60*24
        static let week: TimeInterval = 60*60*24*7
        static let month: TimeInterval = 60*60*24*30
    }

    func relativeTimeFor(timeInterval: TimeInterval) -> String {
        switch timeInterval {
        case ..<Time.hour: return "An hour ago"
        case Time.hour..<Time.day: return "A day ago"
        case Time.day..<Time.week: return "A few days ago"
        case Time.week..<Time.month: return "A few weeks ago"
        case Time.month..<Time.month*11: return "A few months ago"
        default: return "Not in a while"
        }
    }
}
