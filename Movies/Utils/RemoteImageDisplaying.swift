import UIKit

protocol RemoteImageDisplaying: class {
    var remoteImageTrackingId: String? { get set }
    func loaded(image: UIImage?)
}
