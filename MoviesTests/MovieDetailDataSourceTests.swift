import XCTest
@testable import Movies;

class MovieDetailDataSourceTests: XCTestCase {
    var mockMovieController: MockMovieController!
    var mockImageCache: MockImageCache!
    var dataSource: MovieDetailDataSource!

    override func setUp() {
        super.setUp()

        mockMovieController = MockMovieController()
        mockImageCache = MockImageCache()
        dataSource = MovieDetailDataSource(movieName: "test movie", movieController: mockMovieController, imageCache: mockImageCache)
    }

    func test_initialState_isLoadingWithTitleAndZeroScore() {
        mockMovieController.delay = 0.1

        if case .loading(let items) = dataSource.state {
            XCTAssertEqual(items, [
                .title("test movie"),
                .score(0)
            ])
        }
        else {
            XCTFail("Wrong initial state")
        }
    }

    func test_loadedState_withNoActors() {
        mockMovieController.details = MovieDetail(name: "name from details", summary: "summary", score: 5, actors: nil)
        dataSource.loadDetails()

        if case .loaded(let items) = dataSource.state {
            XCTAssertEqual(items, [
                .title("name from details"),
                .score(5),
                .text("summary")
            ])
        }
        else {
            XCTFail("Wrong state")
        }
    }

    func test_loadedState_withActors() {
        let actors = [
            Actor(name: "actor 1", imageUrl: "http://www.actor.test", age: 25),
            Actor(name: "actor 2", imageUrl: "http://www.actor2.test", age: 50)
        ]
        mockMovieController.details = MovieDetail(name: "title with cast", summary: "other summary", score: 3.5, actors: actors)
        dataSource.loadDetails()

        if case .loaded(let items) = dataSource.state {
            XCTAssertEqual(items, [
                .title("title with cast"),
                .score(3.5),
                .text("other summary"),
                .text("Cast"),
                .actor(actors[0]),
                .actor(actors[1])
            ])
        }
        else {
            XCTFail("Wrong state")
        }
    }

    func test_loadingImage_clearsImageFirst() {
        let view = MockRemoteImageDisplaying()
        mockImageCache.images = [
            "url1": UIImage(),
        ]

        dataSource.loadImageFor(url: URL(string: "url1"), in: view)

        XCTAssertEqual(view.loadedImages, [nil, mockImageCache.images["url1"]])
    }

    func test_loadingMultipleImagesInSameView_cancelsPreviousLoads() {
        let view = MockRemoteImageDisplaying()
        mockImageCache.delay = 0.05
        mockImageCache.images = [
            "url1": UIImage(),
            "url2": UIImage()
        ]

        dataSource.loadImageFor(url: URL(string: "url1"), in: view)
        mockImageCache.delay = nil
        dataSource.loadImageFor(url: URL(string: "url2"), in: view)

        XCTAssertEqual(view.loadedImages, [nil, nil, mockImageCache.images["url2"]])
    }
}
