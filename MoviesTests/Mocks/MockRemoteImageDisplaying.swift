import UIKit
@testable import Movies

class MockRemoteImageDisplaying: RemoteImageDisplaying {
    var loadedImages: [UIImage?] = []
    var remoteImageTrackingId: String?

    func loaded(image: UIImage?) {
        loadedImages.append(image)
    }
}
