import UIKit
@testable import Movies;

class MockMovieController: NSObject, MovieControlling {
    var delay: Double?
    var movies: [Movie] = []
    var details: MovieDetail?
    private(set) var getMoviesInvocations: Int = 0

    func getMovies(_ callback: (([Movie]?) -> Void)? = nil) {
        self.getMoviesInvocations += 1
        if let delay = delay {
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                callback?(self.movies)
            }
        }
        else {
            callback?(movies)
        }
    }

    func getDetailsForMovie(_ name: String, callback: ((MovieDetail?) -> Void)? = nil) {
        if let delay = delay {
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                callback?(self.details)
            }
        }
        else {
            callback?(details)
        }
    }
}
