import UIKit
import Services
@testable import Movies

class MockImageCache: ImageCaching {
    var delay: Double?
    var images: [String: UIImage] = [:]

    func imageFor(url: URL, callback: @escaping (UIImage?) -> Void) {
        if let delay = delay {
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                callback(self.images[url.absoluteString])
            }
        }
        else {
            callback(images[url.absoluteString])
        }
    }
}
