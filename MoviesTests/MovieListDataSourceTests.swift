import XCTest
@testable import Movies;

class MovieListDataSourceTests: XCTestCase {
    
    func test_refreshingMultipleTimes_queriesControllerOnce() {
        let mockMovieController = MockMovieController()
        mockMovieController.delay = 1
        let dataSource = MovieListDataSource(movieController: mockMovieController)

        dataSource.refresh()
        dataSource.refresh()
        dataSource.refresh()

        XCTAssertEqual(mockMovieController.getMoviesInvocations, 1)
    }

    func test_gettingMovies_returnsUnderlyingMovieControllerMovies() {
        let mockMovieController = MockMovieController()
        mockMovieController.movies = [
            Movie(name: "0", lastUpdated: 0),
            Movie(name: "1", lastUpdated: 1)
        ]
        let dataSource = MovieListDataSource(movieController: mockMovieController)

        dataSource.refresh()

        if case let .loaded(movies) = dataSource.state {
            XCTAssertEqual(movies.count, 2)
            XCTAssertEqual(movies[0].name, "0")
            XCTAssertEqual(movies[1].name, "1")
        }
        else {
            XCTFail("Wrong number of movies returned")
        }
    }

    func test_gettingMovies_returnsUnderlyingMovieControllerMovies_whenNil() {
        let mockMovieController = MockMovieController()
        let dataSource = MovieListDataSource(movieController: mockMovieController)

        dataSource.refresh()

        if case let .loaded(movies) = dataSource.state {
            XCTAssertEqual(movies.count, 0)
        }
        else {
            XCTFail("Wrong number of movies returned")
        }
    }
}
