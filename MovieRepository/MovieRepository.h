#import <Foundation/Foundation.h>

//! Project version number for MovieRepository.
FOUNDATION_EXPORT double MovieRepositoryVersionNumber;

//! Project version string for MovieRepository.
FOUNDATION_EXPORT const unsigned char MovieRepositoryVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MovieRepository/PublicHeader.h>
#import "MovieController.h"

