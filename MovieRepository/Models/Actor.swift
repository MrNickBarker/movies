import Foundation

@objc
public class Actor: NSObject {
    @objc public let name: String
    @objc public let imageUrl: URL?
    @objc public let age: Int

    @objc
    public init(name: String, imageUrl: String?, age: Int) {
        self.name = name
        if let imageUrl = imageUrl {
            self.imageUrl = URL(string: imageUrl)
        }
        else {
            self.imageUrl = nil
        }
        self.age = age
        super.init()
    }
}
