import Foundation

@objc
public class Movie: NSObject {
    @objc public let name: String
    @objc public let lastUpdated: TimeInterval

    @objc public init(name: String, lastUpdated: TimeInterval) {
        self.name = name
        self.lastUpdated = lastUpdated
    }
}
