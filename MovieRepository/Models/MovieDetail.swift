import Foundation

@objc
public class MovieDetail: NSObject {
    @objc public let name: String
    @objc public let summary: String?
    @objc public let score: Float
    @objc public let actors: [Actor]?

    @objc
    public init(name: String, summary: String?, score: Float, actors: [Actor]?) {
        self.name = name
        self.summary = summary
        self.score = score
        self.actors = actors
    }
}
