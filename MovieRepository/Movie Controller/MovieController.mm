#import "MovieController.h"
#import "MovieController_Internal.h"
#import "MovieControllerCPP.hpp"
#import <MovieRepository/MovieRepository-Swift.h>

@interface MovieController()
{
    movies::MovieController *_movieController;
}
@end

@implementation MovieController

- (instancetype)init {
    self = [super init];
    if (self) {
        _movieController = new movies::MovieController();
    }
    return self;
}

- (void)dealloc {
    delete _movieController;
}

- (void)getMovies:(void (^)(NSArray<Movie *> * _Nullable))callback {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        std::vector<movies::Movie *> cppMovies = self->_movieController->getMovies();

        NSMutableArray *movies = [NSMutableArray array];
        std::for_each(cppMovies.begin(), cppMovies.end(), ^(movies::Movie *movie) {
            [movies addObject:[[Movie alloc] initWithName:[NSString stringWithUTF8String:movie->name.c_str()]
                                              lastUpdated:movie->lastUpdated]];
        });

        dispatch_async(dispatch_get_main_queue(), ^{
            callback([movies copy]);
        });
    });
}

- (void)getDetailsForMovie:(NSString *)name callback:(void (^)(MovieDetail * _Nullable))callback {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        movies::MovieDetail *cppDetails = self->_movieController->getMovieDetail([name UTF8String]);
        if (cppDetails == NULL) {
            callback(nil);
            return;
        }

        NSMutableArray *actors = [NSMutableArray array];
        std::for_each(cppDetails->actors.begin(), cppDetails->actors.end(), ^(movies::Actor actor) {
            [actors addObject:[[Actor alloc] initWithName:[NSString stringWithUTF8String:actor.name.c_str()]
                                                 imageUrl:[NSString stringWithUTF8String:actor.imageUrl.c_str()]
                                                      age:actor.age]];
        });

        MovieDetail *movieDetail = [[MovieDetail alloc] initWithName:[NSString stringWithUTF8String:cppDetails->name.c_str()]
                                                             summary:[NSString stringWithUTF8String:cppDetails->description.c_str()]
                                                               score:cppDetails->score
                                                              actors:actors];

        dispatch_async(dispatch_get_main_queue(), ^{
            callback(movieDetail);
        });
    });
}

@end

@implementation MovieController (Internal)

- (void)setMovieController:(movies::MovieController *)movieController {
    if (_movieController != NULL) {
        delete _movieController;
    }
    _movieController = movieController;
}

@end
