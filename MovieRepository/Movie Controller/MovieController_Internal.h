#include "MovieController.h"

#ifdef __cplusplus
namespace movies {
    class MovieController;
}
#endif

#ifndef MovieController_Internal_h
#define MovieController_Internal_h

@interface MovieController (Internal)
- (void)setMovieController:(movies::MovieController *)movieController;
@end

#endif /* MovieController_Internal_h */
