#import <Foundation/Foundation.h>
@class Movie;
@class MovieDetail;

#ifndef MovieController_h
#define MovieController_h

@protocol MovieControlling <NSObject>
- (void)getMovies:(nullable void(^)(NSArray<Movie*> *_Nullable results))callback;
- (void)getDetailsForMovie:(nonnull NSString *)name callback:(nullable void(^)(MovieDetail *_Nullable detail))callback;
@end

@interface MovieController: NSObject <MovieControlling>
@end

#endif
